/**
 * BibSonomy-UserBackup - The Backup Client.
 *
 * Copyright (C) 2006 - 2014 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.bibsonomy.userbackup;

import static org.bibsonomy.util.ValidationUtils.present;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.log4j.PropertyConfigurator;
import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.bibsonomy.rest.client.RestLogicFactory;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

/**
 * Main dialog of userbackup
 * 
 * @author Michael Wagner
 */
public class BackupMain {
	private static final String DEFAULT_PATH = System.getProperty("user.dir");
	
	private static final String CONFIG_FILE_NAME = "backup_config.xml";
	private static final String CONFIG_USERNAME_ATTRIBUTE_NAME = "username";
	private static final String CONFIG_API_KEY_ATTRIBUTE_NAME = "apikey";
	private static final String CONFIG_PATH_ATTRIBUTE_NAME = "path";
	private static final String CONFIG_API_URL_ATTRIBUTE_NAME = "apiurl";
	private static final String CONFIG_BACKUP_USER_ATTRIBUTE_NAME = "backupUser";
	private static final String CONFIG_GROUPING_ATTRIBUTE_NAME = "grouping";
	
	/**
	 * method to call when starting backup or restore
	 * 
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		// init logging
		PropertyConfigurator.configureAndWatch("log4j.properties", 60 * 1000);

		// create commandline-parser
		final CommandLineParser parser = new GnuParser();
		final Options options = getBibUserBackupOptions();

		// generate the help statement
		final HelpFormatter formatter = new HelpFormatter();

		try {
			// parse the command line arguments
			final CommandLine line = parser.parse(options, args);
			// show help
			// we need at least the w or r option
			if (!line.hasOption("w") && !line.hasOption("r")
					|| line.hasOption("w") && line.hasOption("r")
					|| line.hasOption("-help")) {
				formatter.printHelp("bibsonomy-userbackup", "PARAMETER -w OR -r REQUIRED, OTHERS ARE OPTIONAL", options, "\n");
				return;
			}
			
			final BackupConfig config = readConfig(line);
			final Backup backup = new Backup(config);
			
			// backup data
			if (line.hasOption("w")) {
				backup.downloadBackup();
			}
			// restore backup
			else if (line.hasOption("r")) {
				backup.uploadBackup();
			} else if (line.hasOption("wc")) {
				writeConfig(config, normalizePath(line.getOptionValue("wc")));
			}

		} catch (ParseException e) {
			// print error message
			System.err.println("Parsing failed: " + e.getMessage());
		}
	}

	/**
	 * @param config
	 * @param path 
	 */
	private static void writeConfig(BackupConfig config, String path) {
		try {
			final FileOutputStream fo = new FileOutputStream(path + CONFIG_FILE_NAME);
			final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			final DocumentBuilder loader = factory.newDocumentBuilder();
			final Document document = loader.newDocument();
			
			final Element newElement = document.createElement("userconfig");
			newElement.setAttribute(CONFIG_USERNAME_ATTRIBUTE_NAME, config.getUsername());
			newElement.setAttribute(CONFIG_API_KEY_ATTRIBUTE_NAME, config.getApikey());
			newElement.setAttribute(CONFIG_PATH_ATTRIBUTE_NAME, config.getPath());
			newElement.setAttribute(CONFIG_API_URL_ATTRIBUTE_NAME, config.getApiUrl());
			newElement.setAttribute(CONFIG_BACKUP_USER_ATTRIBUTE_NAME, config.getBackupUser());
			newElement.setAttribute(CONFIG_GROUPING_ATTRIBUTE_NAME, config.getGroupingType().getStringRepresentation());
			document.appendChild(newElement);
			
			final OutputFormat format = new OutputFormat(document);
			
			format.setIndenting(true);
			format.setIndent(4);
			
			final XMLSerializer serializer = new XMLSerializer(format);
			
			serializer.setOutputByteStream(fo);
			serializer.serialize(document);
			fo.close();
		} catch (IOException | ParserConfigurationException e1) {
			System.out.println(e1.getMessage());
		}
	}

	/**
	 * 
	 * @param line
	 * @return a BibUserBackupConfig
	 */
	public static BackupConfig readConfig(CommandLine line) {
		/* check for config file */
		if (line.hasOption("p")) {
			final String path = normalizePath(line.getOptionValue('p'));
			return loadConfigFromConfigFile(path);
		}
		
		final BackupConfig config = new BackupConfig();
		
		/*
		 * the user to backup
		 */
		final String userToBackup;
		if (line.hasOption('u')) {
			userToBackup = line.getOptionValue('u');
		} else {
			userToBackup = IOUtils.getCorrectKeyBoardInput("username");
		}
		config.setBackupUser(userToBackup);
		
		/*
		 * get the user to backup
		 */
		String apiUserName;
		if (line.hasOption("au")) {
			apiUserName = line.getOptionValue("au");
		} else {
			System.out.println("Please enter the API user name: [default: " + userToBackup + "]");
			apiUserName = IOUtils.getKeyBoardInput();
			if (!present(apiUserName)) {
				apiUserName = userToBackup;
			}
		}
		config.setUsername(apiUserName);
		
		/*
		 * get the api key
		 */
		final String apiKey;
		if (line.hasOption('k')) {
			apiKey = line.getOptionValue('k');
		} else {
			apiKey = IOUtils.getCorrectKeyBoardInput("API key");
		}
		config.setApikey(apiKey);
		
		/*
		 * the api url to use
		 */
		String api_url;
		if (line.hasOption('h')) {
			api_url = line.getOptionValue('h');
		} else {
			System.out.println("Please enter the API URL: [default: " + RestLogicFactory.BIBSONOMY_API_URL + "]");
			api_url = IOUtils.getKeyBoardInput();
			if (!present(api_url)) {
				System.out.println("Using default API URL: " + RestLogicFactory.BIBSONOMY_API_URL);
				api_url = RestLogicFactory.BIBSONOMY_API_URL;
			}
		}
		config.setApiUrl(api_url);
		
		if (line.hasOption("w")) {
			getExtraInformationDownload(line, config);
		}
		if (line.hasOption("r")) {
			getExtraInformationUpload(line, config);
		}
		return config;
	}

	/**
	 * @param path
	 * @return
	 */
	private static BackupConfig loadConfigFromConfigFile(String pathToConfigFile) {
		try {
			final FileInputStream fi = new FileInputStream(pathToConfigFile);
			final DocumentBuilderFactory factory  = DocumentBuilderFactory.newInstance();
			final DocumentBuilder builder = factory.newDocumentBuilder();
			final Document document = builder.parse(fi);
			final Element el = document.getDocumentElement();
			final BackupConfig config = new BackupConfig();
			config.setUsername(el.getAttributeNode(CONFIG_USERNAME_ATTRIBUTE_NAME).getValue());
			config.setApikey(el.getAttributeNode(CONFIG_API_KEY_ATTRIBUTE_NAME).getValue());
			final Attr pathAttribute = el.getAttributeNode(CONFIG_PATH_ATTRIBUTE_NAME);
			final String path;
			if (present(pathAttribute)) {
				path = normalizePath(pathAttribute.getValue());
			} else {
				path = normalizePath(DEFAULT_PATH);
			}
			config.setPath(path);
			
			final Attr apiUrlAttribute = el.getAttributeNode(CONFIG_API_URL_ATTRIBUTE_NAME);
			String apiUrl = null;
			if (present(apiUrlAttribute)) {
				apiUrl = apiUrlAttribute.getValue();
			}
			if (!present(apiUrl)) {
				apiUrl = RestLogicFactory.BIBSONOMY_API_URL;
			}
			config.setApiUrl(apiUrl);
			final Attr backupUserAttribute = el.getAttributeNode(CONFIG_BACKUP_USER_ATTRIBUTE_NAME);
			String backupUser = null;
			if (present(backupUserAttribute)) {
				backupUser = backupUserAttribute.getValue();
			}
			if (!present(backupUser)) {
				backupUser = config.getUsername();
			}
			config.setBackupUser(backupUser);
			
			BackupGrouping grouping = BackupGrouping.ALL;
			final Attr groupingAttribute = el.getAttributeNode(CONFIG_GROUPING_ATTRIBUTE_NAME);
			if (present(groupingAttribute)) {
				final String groupingString = groupingAttribute.getValue();
				if (present(groupingString)) {
					grouping = BackupGrouping.convertFromString(groupingString);
				}
			}
			config.setGroupingType(grouping);
			
			fi.close();
			return config; // TODO: we also have to check the config
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * @param line
	 * @param config
	 * @param savePath - path where backup will be saved
	 * @param backupName - name of the user who's backed up
	 * @param groupingType
	 */
	private static void getExtraInformationDownload(CommandLine line, BackupConfig config) {
		if (!line.hasOption("p")) {
			System.out.println("Please enter the path to the folder where your files will be saved: [default: " + DEFAULT_PATH + "]");
			String savePath = normalizePath(IOUtils.getKeyBoardInput());
			if (!present(savePath)) {
				savePath = normalizePath(DEFAULT_PATH);
			}
			config.setPath(savePath);
		}
		
		BackupGrouping groupingType = BackupGrouping.ALL;
		if (line.hasOption("g")) {
			groupingType = BackupGrouping.convertFromString(line.getOptionValue("g"));
		} else{
			boolean success = false;
			while (!success) {
				System.out.println("Choose posts to perform backup:");
				System.out.println("[1] private posts");
				System.out.println("[2] public posts");
				System.out.println("[3] all posts");
				final String input = IOUtils.getKeyBoardInput();
				try {
					
					int number = Integer.parseInt(input);
					success = true;
					switch (number) {
					case 1:
						groupingType = BackupGrouping.PRIVATE;
						break;
					case 2:
						groupingType = BackupGrouping.PUBLIC;
						break;
					case 3:
						groupingType = BackupGrouping.ALL;
						break;
					default:
						success = false;
						break;
					}
				} catch (NumberFormatException e) {
					// ignore
				}
				if (!success) {
					System.out.println("Invalid input");
				}
			}
		}
		config.setGroupingType(groupingType);
	}

	private static void getExtraInformationUpload(CommandLine line, BackupConfig config) {
		String filePath;
		if (!line.hasOption("p")) {
			System.out.println("Please enter the path to your backup-file: [default: " + normalizePath(DEFAULT_PATH) + config.getUsername() + ".zip]");
			filePath = IOUtils.getKeyBoardInput();
			
			// default is the path to your current folder + your "username.zip
			if (filePath.length() == 0) {
				filePath = config.getPath() + config.getUsername() + ".zip";
			}
		} else {
			filePath = normalizePath(line.getOptionValue("p")) + line.getOptionValue("u") + ".zip";
		}
		config.setPath(filePath);
		if (!line.hasOption("p") || !line.hasOption("u") || !line.hasOption("g")) {
			config.setAsk(true);
		}
	}

	/**
	 * 
	 * Method checks if a path ends with "/", if not puts a "/" at the end of
	 * the path.
	 * 
	 * @param path
	 *            - The path to change.
	 * 
	 * @return The changed path.
	 * 
	 */
	public static String normalizePath(String path) {
		// path must end with "/" or "\"
		if (path.length() > 0) {
			if (!String.valueOf(path.charAt(path.length() - 1)).equals("/")
					&& path.indexOf("/") != -1) {
				path += "/";
			}
			if (!String.valueOf(path.charAt(path.length() - 1)).equals("\\")
					&& path.indexOf("\\") != -1) {
				path += "\\";
			}
			if (!String.valueOf(path.charAt(path.length() - 1)).equals("\\")
					&& path.indexOf(":") != -1) {
				path += "\\";
			}

		}
		return path;
	}

	private static Options getBibUserBackupOptions() {
		final Options bibUserBackupOptions = new Options();
		bibUserBackupOptions.addOption("w", "backup", false, "write a backup");
		bibUserBackupOptions.addOption("r", "restore", false, "restore a backup");
		bibUserBackupOptions.addOption("wc", "writeConfig", true, "write config passed to this program to a file");
		bibUserBackupOptions.addOption("help", "help", false, "print this message");
		bibUserBackupOptions.addOption("h", "host", false, "the API url of the system");
		bibUserBackupOptions.addOption("p", "path", true, "the path to the config-file");
		bibUserBackupOptions.addOption("u", "user", true, "the name of the user who will be back-uped\n"+
				"(in case of a restore the file 'username.zip' must\n " +
				" exist in the path which is specified in the config-file)");
		bibUserBackupOptions.addOption("k", "apikey", true, "the api key used for authentication");
		bibUserBackupOptions.addOption("au", "apiuser", false, "the user name used for authentication if emtpy passed argument to user is used");
		bibUserBackupOptions.addOption("g", "grouping", true, "the grouping of the posts you want to backup: " +
				"\ntype 'pub', 'prv' or 'all'");
		return bibUserBackupOptions;
	}
}
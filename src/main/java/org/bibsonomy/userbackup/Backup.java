/**
 * BibSonomy-UserBackup - The Backup Client.
 *
 * Copyright (C) 2006 - 2014 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.bibsonomy.userbackup;

import static org.bibsonomy.util.ValidationUtils.present;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

import org.bibsonomy.common.enums.GroupingEntity;
import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.Bookmark;
import org.bibsonomy.model.Document;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.Resource;
import org.bibsonomy.model.User;
import org.bibsonomy.model.enums.Order;
import org.bibsonomy.model.logic.LogicInterface;
import org.bibsonomy.model.util.GroupUtils;
import org.bibsonomy.model.util.data.NoDataAccessor;
import org.bibsonomy.rest.client.RestLogicFactory;
import org.bibsonomy.rest.exceptions.BadRequestOrResponseException;
import org.bibsonomy.rest.renderer.Renderer;
import org.bibsonomy.rest.renderer.RendererFactory;
import org.bibsonomy.rest.renderer.RenderingFormat;
import org.bibsonomy.rest.renderer.UrlRenderer;

/**
 * backups all details and posts of a user
 * 
 * @author Michael Wagner
 */
public class Backup {
	private static final String YES = "y";
	
	private static final String PUBLICATIONS = "publication";
	private static final String BOOKMARKS = "bookmark";
	
	private static final String[] STATUS = { "|", "/", "-", "\\" };
	private static final int STEP_SIZE = 100;
	
	private LogicInterface logic;
	private final BackupConfig config;
	
	/**
	 * @param config
	 */
	public Backup(final BackupConfig config) {
		this.config = config;
		final RestLogicFactory rlf = new RestLogicFactory(config.getApiUrl());
		logic = rlf.getLogicAccess(config.getUsername(), config.getApikey());
	}

	/**
	 * gets the userdetails
	 * 
	 * @param username name of a user
	 * @return returns false if user with name username doesnt exist
	 */
	private User getUserDetails(String username) {
		try {
			return logic.getUserDetails(username);
		} catch (Exception e) {
			System.out.println("ERROR: Fail to read user-details");
			return null;
		}
	}

	/**
	 * 
	 * Method gets a list of all posts using the API
	 * 
	 * @param username name of a user
	 * @param type  the type of the resource
	 * @param group
	 *            can be private or public (not implemented causing a api bug)
	 * 
	 */
	private <T extends Resource> List<Post<? extends Resource>> getUserPosts(String username, Class<T> type, final String resourceName, BackupGrouping group) {
		@SuppressWarnings({ "unchecked", "rawtypes" }) // TODO: fix
		final List<Post<? extends Resource>> posts = new LinkedList();
		int numberOfPosts = 0;
		int j = 0;
		boolean success = true;

		GroupingEntity grouping = null;
		String groupingName = null;
		// only private, public or all posts
		switch (group) {
		case PRIVATE:
			grouping = GroupingEntity.VIEWABLE;
			groupingName = GroupUtils.getPrivateGroup().getName();
			break;
		case PUBLIC:
			grouping = GroupingEntity.VIEWABLE;
			groupingName = GroupUtils.getPublicGroup().getName();
			break;
		case ALL:
			grouping = GroupingEntity.USER;
			groupingName = username;
			break;
		default:
			break;
		}
		
		int offset = 0;
		int totalNumberOfPosts = 0;
		do { 
			try {
				final List<Post<T>> downloadedPosts = logic.getPosts(type, grouping, groupingName, null, null, null, null, Order.ADDED, null, null, offset, offset + STEP_SIZE);
				numberOfPosts = downloadedPosts.size();
				totalNumberOfPosts += numberOfPosts;
				
				posts.addAll(downloadedPosts);
				writeProgressStatustoConsole(j++);
			} catch (BadRequestOrResponseException e) {
				success = false;
				System.out.println(e.getMessage());
			}
			offset += STEP_SIZE;
		} while (numberOfPosts == STEP_SIZE);
		
		if (success) {
			System.out.println(totalNumberOfPosts + " " + resourceName + " post(s) found on server");
		}
		return posts;
	}

	/**
	 * Method uses BibUserBackupOutput to write data to a zip file.
	 * 
	 * @throws IOException
	 */
	public void downloadBackup() {
		final String userToBackup = config.getBackupUser();
		final User userDetails = getUserDetails(userToBackup);
		// Default all posts
		BackupGrouping grouping = BackupGrouping.PUBLIC;

		if (present(userDetails) && present(userDetails.getName())) {
			// only public posts in case of api-user != backup-user
			if (userToBackup.equals(logic.getAuthenticatedUser().getName())) {
				grouping = config.getGroupingType();
			}
			
			final List<Post<? extends Resource>> publications = getUserPosts(userToBackup, BibTex.class, PUBLICATIONS, grouping);
			final List<Post<? extends Resource>> bookmarks = getUserPosts(userToBackup, Bookmark.class, BOOKMARKS, grouping);
			
			final boolean success = storeBackup(this.config, this.logic, new BackupContent(userDetails, bookmarks, publications));
			if (success) {
				System.out.println("\nSuccess: " + userToBackup + ".zip saved in " + config.getPath());
			}
		} else {
			System.out.println("Error: User '" + config.getBackupUser() + "' does not exist");
		}
	}

	/**
	 * @param config
	 * @param bookmarks
	 * @param publications
	 */
	private static boolean storeBackup(BackupConfig config, LogicInterface logic, BackupContent content) {
		final RendererFactory renderingFactory = new RendererFactory(new UrlRenderer(config.getApiUrl()));
		final Renderer renderer = renderingFactory.getRenderer(RenderingFormat.XML);
		
		try {
			final ZipOutputStream zipOutput = new ZipOutputStream(new FileOutputStream(config.getPath() + config.getBackupUser() + ".zip"));
			
			storeUserInZip(zipOutput, renderer, content.getUser());
			storePosts(zipOutput, logic, renderer, content.getBookmarks(), "bookmark", BOOKMARKS);
			storePosts(zipOutput, logic, renderer, content.getPublications(), "publication", PUBLICATIONS);
			zipOutput.close();
			return true;
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		return false;
	}

	/**
	 * @param zipOutput
	 * @param renderer
	 * @param bookmarks
	 */
	private static void storePosts(ZipOutputStream zipOutput, final LogicInterface logic, Renderer renderer, List<Post<? extends Resource>> posts, final String baseFileName, final String resourceType) throws IOException {
		int i = 0;
		for (final Post<? extends Resource> post : posts) {
			final StringWriter sw = new StringWriter(100);
			renderer.serializePost(sw, post, null);
			
			final ZipEntry entry = new ZipEntry(baseFileName + i + ".xml");
			try {
				zipOutput.putNextEntry(entry);
				zipOutput.write(sw.toString().getBytes(), 0, sw.toString().getBytes().length);
				zipOutput.closeEntry();
				writePostStatustoConsole((i + 1), posts.size(), resourceType);
			} catch (IOException e) {
				e.printStackTrace();
			}
			// TODO: not nice
			if (post.getResource() instanceof BibTex) {
				final BibTex publication = (BibTex) post.getResource();
				final List<Document> documents = publication.getDocuments();
				
				if (documents != null) {
					for (final Document document : documents) {
						final File f = logic.getDocument(post.getUser().toString(), publication.getIntraHash(), document.getFileName()).getFile();
						if (f != null) {
							final ZipEntry fileEntry = new ZipEntry(publication.getIntraHash() + "/" + f.getName().substring(f.getName().lastIndexOf("_") + 1));
							zipOutput.putNextEntry(fileEntry);
							
							final byte[] bytes = new byte[(int) f.length()];
							final FileInputStream fis = new FileInputStream(f);
							int n = 0;
							while ((n=fis.read(bytes, 0, bytes.length)) != -1) {
								zipOutput.write(bytes, 0, n);
							}
							
							zipOutput.closeEntry();
							fis.close();
						}
					}
				}
			}
			i++;
		}
		System.out.println("");
		System.out.println("Successfully saved " + posts.size() + " " + resourceType + " post(s) to disk");
	}
	
	private static void writePostStatustoConsole(int i, int size, String resourceType) {
		System.out.print("\r" + STATUS[i % 4] + " "
				+ String.valueOf(Math.round(((double) i / size) * 100)) + " % saved (" + resourceType + ")");
	}

	/**
	 * @param zipOutput
	 * @param userDetials
	 */
	private static void storeUserInZip(ZipOutputStream zipOutput, Renderer renderer, User userDetials) throws IOException {
		final StringWriter sw = new StringWriter(100);
		renderer.serializeUser(sw, userDetials, null);
		
		final ZipEntry entry = new ZipEntry("userDetails.xml");
		zipOutput.putNextEntry(entry);
		zipOutput.write(sw.toString().getBytes(), 0, sw.toString().length());
		zipOutput.closeEntry();
		System.out.print("written user to backup file");
	}

	/**
	 * 
	 * Method uses BibUserBackupInput to read bibsonomy data from a zip file
	 * 
	 * @throws IOException
	 */
	public void uploadBackup() {
		try {
			final ZipFile zf = new ZipFile(config.getPath());
			final BackupContent content = readBackupContentFromArchive(this.config, zf);
			
			final List<Post<? extends Resource>> bookmarks = content.getBookmarks();
			final List<Post<? extends Resource>> publications = content.getPublications();
			
			if (bookmarks.size() > 0 || publications.size() > 0) {
				final String yesno;
				if (config.isAsk()) {
					System.out.println(bookmarks.size() + " bookmark und " + publications.size() + " publication posts will be insert for user '" + this.config.getBackupUser() + "'. continue? [y/N]");
					yesno = IOUtils.getKeyBoardInput();
				} else {
					yesno = YES;
				}
				// do upload
				if (YES.equals(yesno)) {
					restorePosts(zf, logic, publications, PUBLICATIONS);
					restorePosts(zf, logic, bookmarks, BOOKMARKS);
				} else {
					System.out.println("Abort");
				}
			} else {
				System.out.println("No posts to restore.");
			}
			zf.close();
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}

	private static void restorePosts(final ZipFile file, final LogicInterface logic, List<Post<? extends Resource>> posts, final String resourceType) throws IOException {
		logic.createPosts(posts);
		
		for (Post<? extends Resource> post : posts) {
			if (!(post.getResource() instanceof BibTex)) {
				break;
			}
			final BibTex publication = (BibTex) post.getResource();
			final List<Document> documents = publication.getDocuments();
			if (documents != null) {
				for (Document d: documents) {
					final ZipEntry entry = file.getEntry(publication.getIntraHash() + "/"+ d.getFileName());
					if (entry != null) {
						File f = new File(d.getFileName());
						InputStream inputstream = file.getInputStream(entry);
						byte[] bytes = new byte[(int)entry.getSize()];
						int n = 0;
						BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(f)); 
						while((n = inputstream.read(bytes, 0, bytes.length)) != -1) 
							bos.write(bytes, 0, n);
						bos.close();
						
						d.setFile(f);
						logic.createDocument(d, publication.getIntraHash());
						inputstream.close();
					}
				}
			}
		}
		
		System.out.println("Finished insert " + resourceType);
	}

	/**
	 * @param config
	 * @param zf 
	 * @return
	 */
	private static BackupContent readBackupContentFromArchive(BackupConfig config, ZipFile zf) {
		final RendererFactory factory = new RendererFactory(new UrlRenderer(config.getApiUrl()));
		final Renderer renderer = factory.getRenderer(RenderingFormat.XML);
		final List<Post<? extends Resource>> bookmarks = readPosts(zf, renderer, new String[] { "bookmark" });
		final List<Post<? extends Resource>> publications = readPosts(zf, renderer, new String[] {"bibtex", "publication" });
		final User user = readUser(zf, renderer);
		return new BackupContent(user, bookmarks, publications);
	}

	/**
	 * @param zf
	 * @param renderer
	 * @return
	 */
	private static User readUser(ZipFile zf, Renderer renderer) {
		ZipEntry ze = zf.getEntry("userDetails.xml");

		InputStream input;
		try {
			input = zf.getInputStream(ze);
			InputStreamReader inputreader = new InputStreamReader(input);
			final User user = renderer.parseUser(inputreader);
			inputreader.close();
			return user;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * @param zf
	 */
	private static List<Post<? extends Resource>> readPosts(ZipFile zf, final Renderer renderer, final String[] baseNames) {
		@SuppressWarnings({ "unchecked", "rawtypes" }) // TODO: fix
		final List<Post<? extends Resource>> posts = new LinkedList();
		int i = 0;
		while (true) {
			ZipEntry ze = null;
			
			for (String baseName : baseNames) {
				ze = zf.getEntry(baseName + i + ".xml");
				if (ze != null) {
					break;
				}
			}
			
			if (ze == null) {
				break;
			}
			try {
				final InputStream input = zf.getInputStream(ze);
				final InputStreamReader inputreader = new InputStreamReader(input);
				final Post<? extends Resource> post = renderer.parsePost(inputreader, NoDataAccessor.getInstance());
				posts.add(post);
				inputreader.close();
				writeProgressStatustoConsole(i);
			} catch (IOException e) {
				e.printStackTrace();
			}
			i++;
		}
		return posts;
	}

	private static void writeProgressStatustoConsole(int j) {
		final String pos1 = "\r";
		System.out.print(pos1 + STATUS[(j++) % 4] + " ");
	}
}

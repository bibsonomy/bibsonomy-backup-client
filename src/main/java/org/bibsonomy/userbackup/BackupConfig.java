/**
 * BibSonomy-UserBackup - The Backup Client.
 *
 * Copyright (C) 2006 - 2014 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.bibsonomy.userbackup;


/**
 * class representing the config of the application
 * 
 * @author Michael Wagner
 */
public class BackupConfig {
	/** the username used for authentication */
	private String username;
	private String path;
	private String apikey;
	private String apiUrl;
	/** the user to backup */
	private String backupUser;
	private BackupGrouping groupingType;
	private boolean ask = false;
	
	/**
	 * default constructor
	 */
	public BackupConfig() {
		// noop
	}
	
	/**
	 * constructor filling the path
	 * @param path
	 */
	public BackupConfig(String path){
		this.path = path;
	}
	
	/**
	 * constructor filling fields
	 * @param path
	 * @param username
	 * @param apiKey
	 * @param apiurl
	 */
	public BackupConfig(String path, String username, String apiKey, String apiurl){
		this.username = username;
		this.apikey = apiKey;
		this.path = path;
		this.apiUrl = apiurl;
	}
	
	/**
	 * @return the username
	 */
	public String getUsername() {
		return this.username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the path
	 */
	public String getPath() {
		return this.path;
	}

	/**
	 * @param path the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * @return the apikey
	 */
	public String getApikey() {
		return this.apikey;
	}

	/**
	 * @param apikey the apikey to set
	 */
	public void setApikey(String apikey) {
		this.apikey = apikey;
	}

	/**
	 * @return the apiurl
	 */
	public String getApiUrl() {
		return this.apiUrl;
	}

	/**
	 * @param apiurl the apiurl to set
	 */
	public void setApiUrl(String apiurl) {
		this.apiUrl = apiurl;
	}

	/**
	 * @return the backupUser
	 */
	public String getBackupUser() {
		return this.backupUser;
	}

	/**
	 * @param backupUser the backupUser to set
	 */
	public void setBackupUser(String backupUser) {
		this.backupUser = backupUser;
	}

	/**
	 * @return the groupingType
	 */
	public BackupGrouping getGroupingType() {
		return this.groupingType;
	}

	/**
	 * @param groupingType the groupingType to set
	 */
	public void setGroupingType(BackupGrouping groupingType) {
		this.groupingType = groupingType;
	}

	/**
	 * @return the ask
	 */
	public boolean isAsk() {
		return this.ask;
	}

	/**
	 * @param ask the ask to set
	 */
	public void setAsk(boolean ask) {
		this.ask = ask;
	}
}

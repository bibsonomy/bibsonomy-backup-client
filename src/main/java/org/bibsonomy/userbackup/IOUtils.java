/**
 * BibSonomy-UserBackup - The Backup Client.
 *
 * Copyright (C) 2006 - 2014 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.bibsonomy.userbackup;

import static org.bibsonomy.util.ValidationUtils.present;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * common io interaction helper methods
 *
 * @author dzo
 */
public class IOUtils {
	
	/**
	 * read from standard in the first line
	 * @return the read line
	 */
	public static String getKeyBoardInput() {
		final BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		try {
			return br.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "";
	}
	
	/**
	 * reads from standard in as long as the user does not provide anything
	 * @param informationAsked
	 * @return the valid read line
	 */
	public static String getCorrectKeyBoardInput(String informationAsked) {
		String input = "";
		while (true) {
			System.out.println("Please enter your " + informationAsked);
			input = getKeyBoardInput();
			if (present(input)) {
				return input;
			}
		}
	}
}
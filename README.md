# What is it?

BibSonomy UserBackup is a standalone application with which you can backup
all your posts from BibSonomy and later restore them.

The module is part of the [BibSonomy](http://www.bibsonomy.org) social
bookmarking system and is maintained by the
[Knowledge & Data Engineering Group](http://www.kde.cs.uni-kassel.de/)
at the University of Kassel, Germany,
the [Data Mining and Information Retrieval Group](http://www.is.informatik.uni-wuerzburg.de/en/dmir/)
at the University of WÃ¼rzburg, Germany, and
the [L3S Research Center](http://www.l3s.de/)
at Leibniz University Hannover, Germany.

# Documentation

The documentation available as of the date of this release is included in
the form of JavaDoc annotations in the source code.

# System Requirements

- JDK: 1.7 or above.
- Memory: No minimum requirement.
- Disk: No minimum requirement.
- Operating System: No minimum requirement.

# Licensing

Please see the file [LICENSE.txt](https://bitbucket.org/bibsonomy/bibsonomy-backup-client/src/tip/LICENSE.txt?at=stable).

# Maven URLS

- [Home Page](http://dev.bibsonomy.org/)
- [Maven Repository](http://dev.bibsonomy.org/maven2/)
- [Issue Tracking](https://bitbucket.org/bibsonomy/bibsonomy-backup-client/issues?status=new&status=open)
